# Shopify Connect

The goal of this project is to submit a Pull Request on https://bitbucket.org/mattcometly/demo/src/master/. We use this repo to prototype new features.

The new feature you’ll be working on is Shopify integration. Please spend as much time as you want, but payment is limited to 5 hours. Because this is a trial project, we ask that you consider not charging us if you feel you are unable to deliver a satisfactory result. In other words, please be mindful of your own time and abilities.

## Tasksx

1. Fork this repo
1. Create a Shopfiy test store for yourself. You'll need to add a couple sample products. You'll need it for the following steps.
1. We have a public shopify app used for testing. Contact us for our Cometly Testbed API Key and Secret.
1. In Laravel, create an authenticated page called `/profile` with a `Connect to Shopfiy` button on it.
1. When the authenticated Laravel user clicks that button, it should allow him to connect to his Shopify account via oAuth.
   1. https://shopify.dev/tutorials/authenticate-with-oauth
   2. This is a great resource: https://www.shopify.com/partners/blog/17056443-how-to-generate-a-shopify-api-token.
1. Store the user’s Shopify oAuth credential in the database into `user_extra.shopify_token` using a new Laravel migration
1. Create a new page at `/products` that lists the user’s Shopify products for sale.
   - Load current stores via an XHR request Laravel, which in turn calls the Shopify API with the user’s oAuth credentials.
   - Bonus if you can include pictures.
1. Send a Pull Request from your fork to us, and let us know!
